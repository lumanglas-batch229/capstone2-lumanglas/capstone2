const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, "firstname is required"]
	},
	lastName : {
		type : String,
		required : [true, "lastName is required"]
	},
	email : {
		type : String,
		required : [true, "email is required"]
	},
	password : {
		type : String,
		required : [true, "password is required"]
	},
	isActive : {
		type : Boolean,
		default : true
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	mobileNo : {
		type : String,
		required : [true, "Mobile Number is required"]
	}
	
})

module.exports = mongoose.model("User", userSchema);