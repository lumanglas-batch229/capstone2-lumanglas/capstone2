const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userController.js");
const auth = require("../auth");


// User Registrations
router.post("/register", (req, res) => {
	userControllers.registerUser(req.body).then(resultFromController => 
	res.send(resultFromController));
})

// Login User
router.post("/login", (req, res) => {
	userControllers.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Authenticate or check the user if exist
router.post("/checkEmailAdd", (req, res) => {
	userControllers.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/details", auth.verify, (req, res) => {
	
	// uses the decode method defined in the auth.js to retrieve user info from request header
	const userData = auth.decode(req.headers.authorization)

	userControllers.getProfile({userId: req.body.id}).then(resultFromController => res.send(resultFromController));
})

router.get("/", (req, res) => {

	userControllers.getAllUsers().then(resultFromController => res.send(resultFromController));
})

router.put("/:userId", auth.verify, (req, res) => {

	userControllers.updateUser(req.params, req.body).then(resultFromController => res.send(resultFromController));
})


module.exports = router;