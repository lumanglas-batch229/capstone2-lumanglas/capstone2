const Product = require("../models/Products");
const User = require("../models/Users");

// Create new product
/*
	Steps:
	1. Create a new Course object using the mongoose model
	2. Save the new Course to the database

*/

module.exports.addProduct = (data) => {

	// User is an admin
	if (data.isAdmin) {

		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price,
			stocks : data.product.stocks
		});

		// Saves the created object to our database
		return newProduct.save().then((product, error) => {

			// Course creation successful
			if (error) {

				return false;

			// Course creation failed
			} else {

				return true;

			};

		});

	// User is not an admin
	} else {
		return false;
	};
	

};

module.exports.getAllActive = () => {

	return Product.find({isActive : true}).then(result => {
		return result;
	});
}


module.exports.getProduct = (reqParams) => {
	console.log(reqParams)
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
}


module.exports.updateProduct = (reqParams, data) => {

	if (data.isAdmin) {

	// specify the fields of the document to be updated
	let updatedProduct = {
		name : data.product.name,
		description: data.product.description,
		price: data.product.price
	};

	// findByIdUpdate(document ID, updatesToBeApplied)
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		// course not updated
			if(error) {
						return false;

		//product is updated
			}else {
					return true;
			};
		});
	}
	else {
		return false;
	};

}

module.exports.archiveProduct = (data) => {

// checking if the user is an admin
if(data.isAdmin === true){

		let updateActiveField = {
			isActive : false
		};

		return Product.findByIdAndUpdate(data.reqParams.productId, updateActiveField).then((product, error) => {

			// Course not archived
			if (error) {

				return false;

			// Course archived successfully
			} else {

				return true;

			}

		});
	} else {
		return false
	}
};




